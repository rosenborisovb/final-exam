package controller;

import exceptions.LoadingException;
import exceptions.SavingExcpetion;
import model.Animal;
import model.AnimalTableModel;
import model.Animals;
import model.FileManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;

public class AnimalsFrame extends JFrame{

    private Animals animals;
    JTable table;
    AnimalTableModel tableModel;

    public AnimalsFrame(){
        super();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100,100,1000,1000);
        setLayout(null);
        animals = new Animals();
        try {
            animals.load();
        } catch (LoadingException e) {

        }

        createUI();
    }

    private void createUI(){


        JButton deleteButton = new JButton("Delete");
        deleteButton.setBounds(10,10,100,30);
        deleteButton.getModel().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                if (table.getSelectedRow() != -1 ) {
                    animals.delete(table.getSelectedRows());
//                }
                tableModel.setAnimals(animals.getAnimals());
                table.updateUI();
            }
        });
        add(deleteButton);

        JButton saveButton = new JButton("Save");
        saveButton.setBounds(120,10,100,30);
        saveButton.getModel().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    save();
                } catch (SavingExcpetion savingExcpetion) {

                }
            }
        });
        add(saveButton);

        JButton loadButton = new JButton("Load");
        loadButton.setBounds(230,10,100,30);
        loadButton.getModel().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    load();
                } catch (LoadingException e1) {

                }
            }
        });
        add(loadButton);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10,60,980,900);
        add(scrollPane);

        table = new JTable();
        tableModel = new AnimalTableModel();
        tableModel.setAnimals(animals.getAnimals());
        table.setModel(tableModel);
        scrollPane.setViewportView(table);


    }

    private void save() throws SavingExcpetion {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showSaveDialog(AnimalsFrame.this) == JFileChooser.APPROVE_OPTION) {
            FileManager.save(animals.getAnimals(), chooser.getSelectedFile());
        }


    }

    private void load() throws LoadingException {
        JFileChooser chooser = new JFileChooser();
        ;
        if (chooser.showOpenDialog(AnimalsFrame.this) == JFileChooser.APPROVE_OPTION) {
            Collection<Animal> loadedAnimals = FileManager.load(chooser.getSelectedFile());
            for (Animal a : loadedAnimals) {
                animals.getAnimals().add(a);
            }
        }
        tableModel.setAnimals(animals.getAnimals());
        table.updateUI();
    }


}
