package model;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class AnimalTableModel extends AbstractTableModel {

    private List<Animal> animals;

    @Override
    public int getRowCount() {
        return animals.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Animal animal = animals.get(rowIndex);
        switch (columnIndex){
            case 0:
                return animal.getDate();
            case 1:
                return animal.getColor();
            case 2:
                return animal.getBreed();
            case 3:
                return animal.getSex();
            case 4:
                return animal.getState();
            case 5:
                return animal.getName();
            case 6:
                return animal.getDateCreated();
            default:
                return null;

        }
    }


    @Override
    public String getColumnName(int column) {
        switch (column){
            case 0:
                return "Date";
            case 1:
                return "Color";
            case 2:
                return "Breed";
            case 3:
                return "Sex";
            case 4:
                return "State";
            case 5:
                return "Name";
            case 6:
                return "Date Created";
            default:
                return null;
        }
    }

    public void setAnimals(List<Animal> animals){
        this.animals = animals;
    }

}
