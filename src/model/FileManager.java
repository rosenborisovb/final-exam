package model;

import exceptions.LoadingException;
import exceptions.SavingExcpetion;

import java.io.*;
import java.util.*;

public class FileManager {

    private static final String SEPARATOR = ",";

    public static Collection<Animal> load(File file) throws LoadingException {
        List<Animal> animals = new ArrayList<>();

        try(
                Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(file)))
                ){
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                Animal animal = createAnimal(line);
                animals.add(animal);
            }
        }catch (IOException ex){
            throw new LoadingException();
        }

        return animals;
    }

    public static void save(Collection<Animal> animals, File file)throws SavingExcpetion{

        try(
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
                ){
            for (Animal a : animals){
                String line = createString(a);
                writer.println(line);
            }
        }catch (IOException ex){
            throw new SavingExcpetion();
        }

    }

    private static String createString(Animal a){

        StringBuilder sb = new StringBuilder();
        sb.append(a.getDate());
        sb.append(SEPARATOR);
        sb.append(a.getColor());
        sb.append(SEPARATOR);
        sb.append(a.getBreed());
        sb.append(SEPARATOR);
        sb.append(a.getSex());
        sb.append(SEPARATOR);
        sb.append(a.getState());
        sb.append(SEPARATOR);
        sb.append(a.getName());
        sb.append(SEPARATOR);
        sb.append(a.getDateCreated());
        return sb.toString();
    }

    private static Animal createAnimal(String line){
        String[] elementsOfAnimal = line.split(SEPARATOR);
        Animal animal = new Animal(elementsOfAnimal[0], elementsOfAnimal[1], elementsOfAnimal[2], elementsOfAnimal[3], elementsOfAnimal[4], elementsOfAnimal[5], elementsOfAnimal[6]);
        return animal;
    }



}
