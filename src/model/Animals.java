package model;

import exceptions.LoadingException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Animals {

    private static final File FILE = new File("res/animals.lostandfound.csv");
    private List<Animal> animals;

    public Animals(){
        animals = new ArrayList<>();
    }

    public void load() throws LoadingException {
        Collection<Animal> loadedAnimals = FileManager.load(FILE);
        for(Animal a : loadedAnimals){
            animals.add(a);
        }
    }



    public List<Animal> getAnimals() {
        return animals;
    }

    public void delete(int[] rows){
        if (rows.length == 1){
            animals.remove(rows[0]);
        }else {
            for (int i = 0; i < rows.length; i ++){
                    animals.remove(rows[i] - i);
            }

        }

    }
}
